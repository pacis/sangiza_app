import React from 'react'
import { NavLink } from 'react-router-dom'
import "./NavLinks.css"

function NavLinks() {
    return (
        <ul className="nav-links">
            <li>
                <NavLink to="/" exact>USERS</NavLink>
            </li>
            <li>    
                <NavLink to="/u1/places">MY PLACES</NavLink>
            </li>
            <li>
                <NavLink to="/places/new">ADD PLACE</NavLink>
            </li>
            <li>
                <NavLink to="/login">LOG IN</NavLink>
            </li>
        </ul>
    )
}

export default NavLinks
