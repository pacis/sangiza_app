import React from 'react'
import { NavLink } from 'react-router-dom'
import "./SideNavLinks.css"

function SideNavLinks(props) {
    return (
        <ul className="side-nav-links">
            <li>
                <NavLink to="/" exact onClick={props.onClick}>USERS</NavLink>
            </li>
            <li>
                <NavLink to="/u1/places" onClick={props.onClick}>MY PLACES</NavLink>
            </li>
            <li>
                <NavLink to="/places/new" onClick={props.onClick}>ADD PLACE</NavLink>
            </li>
            <li>
                <NavLink to="/login" onClick={props.onClick}>LOG IN</NavLink>
            </li>
        </ul>
    )
}

export default SideNavLinks
