import React,{useState} from 'react'
import { Link } from 'react-router-dom'
import Backdrop from '../UIElements/Backdrop'
import MainHeader from './MainHeader'
import "./MainNavigation.css"
import NavLinks from './NavLinks'
import SideDrawer from './SideDrawer'
import SideNavLinks from './SideNavLinks'

function MainNavigation(props) {
    const[drawerIsOpen,setDrawerIsOpen] = useState(false);
    function openDrawer(){
        setDrawerIsOpen(true);
    }

    function closeDrawer(){
        setDrawerIsOpen(false);
    }
    return (
        <>
            {drawerIsOpen && <Backdrop onClick={closeDrawer}/>}
            <SideDrawer show={drawerIsOpen}>
                <nav className="main-navigation__drawer-nav">
                    <SideNavLinks onClick={closeDrawer}/>
                </nav>
            </SideDrawer>
            <MainHeader>
                <button className="main-navigation__menu-btn" onClick={openDrawer}>
                    <span />
                    <span />
                    <span />
                </button>
                <h1 className="main-navigation__title">
                    <Link to="/">Sangiza&nbsp;App</Link>
                </h1>
                <nav className=".main-navigation__header-nav">
                    <NavLinks />
                </nav>
            </MainHeader>
        </>
    )
}

export default MainNavigation
