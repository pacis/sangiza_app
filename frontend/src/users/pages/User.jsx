import React from 'react'
import UsersList from "../components/UsersList"

function User() {
    const USERS = [
        {
            id:"u1",
            name:"Nkubito Pacis",
            image:"https://instagram.fkgl1-1.fna.fbcdn.net/v/t51.2885-15/e35/116494836_285757826048920_7737941272604812055_n.jpg?_nc_ht=instagram.fkgl1-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=L0bOwrhNTywAX-_sEY-&tp=1&oh=83f3d5014dd7d331c3091c3736d3cb6b&oe=6021F954",
            places:3
         },
         {
            id:"u2",
            name:"Stephen Curry",
            image:"https://static.onecms.io/wp-content/uploads/sites/20/2020/04/02/gettyimages-1064769920_1.jpg",
            places:10
         },
         {
            id:"u3",
            name:"Pacis Stellan",
            image:"https://instagram.fkgl1-1.fna.fbcdn.net/v/t51.2885-15/e35/116494836_285757826048920_7737941272604812055_n.jpg?_nc_ht=instagram.fkgl1-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=L0bOwrhNTywAX-_sEY-&tp=1&oh=83f3d5014dd7d331c3091c3736d3cb6b&oe=6021F954",
            places:1
         },
         {
            id:"u4",
            name:"Nkubito Pacis",
            image:"https://specials-images.forbesimg.com/imageserve/5fca2ebc5ae3f701931b61a6/960x0.jpg?fit=scale",
            places:3
         },
         {
            id:"u5",
            name:"Nkubito Pacis",
            image:"https://specials-images.forbesimg.com/imageserve/5fca2ebc5ae3f701931b61a6/960x0.jpg?fit=scale",
            places:3
         },
         {
            id:"u6",
            name:"Pacis Stellan",
            image:"https://instagram.fkgl1-1.fna.fbcdn.net/v/t51.2885-15/e35/116494836_285757826048920_7737941272604812055_n.jpg?_nc_ht=instagram.fkgl1-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=L0bOwrhNTywAX-_sEY-&tp=1&oh=83f3d5014dd7d331c3091c3736d3cb6b&oe=6021F954",
            places:1
         },
         {
            id:"u7",
            name:"Stephen Curry",
            image:"https://static.onecms.io/wp-content/uploads/sites/20/2020/04/02/gettyimages-1064769920_1.jpg",
            places:10
         },
         {
            id:"u8",
            name:"Pacis Stellan",
            image:"https://instagram.fkgl1-1.fna.fbcdn.net/v/t51.2885-15/e35/116494836_285757826048920_7737941272604812055_n.jpg?_nc_ht=instagram.fkgl1-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=L0bOwrhNTywAX-_sEY-&tp=1&oh=83f3d5014dd7d331c3091c3736d3cb6b&oe=6021F954",
            places:1
         },
    ]
    return (
        <>
            <UsersList items={USERS}/> 
        </>
    )
}

export default User
